//
//  ViewController.h
//  IMU
//
//  Created by jarm on 5/16/16.
//  Copyright © 2016 digb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>


@interface ViewController : UIViewController {
	
}

- (IBAction)resetFilter:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *accX;
@property (strong, nonatomic) IBOutlet UILabel *accY;
@property (strong, nonatomic) IBOutlet UILabel *accZ;

@property (strong, nonatomic) IBOutlet UILabel *incRaw;
@property (strong, nonatomic) IBOutlet UILabel *rollRaw;
@property (strong, nonatomic) IBOutlet UILabel *xVar;

@property (strong, nonatomic) IBOutlet UILabel *rotX;
@property (strong, nonatomic) IBOutlet UILabel *rotY;
@property (strong, nonatomic) IBOutlet UILabel *rotZ;

@property (strong, nonatomic) IBOutlet UILabel *rotXsum;
@property (strong, nonatomic) IBOutlet UILabel *rotYsum;
@property (strong, nonatomic) IBOutlet UILabel *rotZsum;


@property (strong, nonatomic) IBOutlet UILabel *xVel;
@property (strong, nonatomic) IBOutlet UILabel *yVel;
@property (strong, nonatomic) IBOutlet UILabel *zVel;


@property (strong, nonatomic) CMMotionManager *motionManager;


@end


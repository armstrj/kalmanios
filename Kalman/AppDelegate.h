//
//  AppDelegate.h
//  IMU
//
//  Created by jarm on 5/16/16.
//  Copyright © 2016 digb. All rights reserved.
//

#import <UIKit/UIKit.h>

@import CoreMotion;

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
	CMMotionManager *motionManager;
}

@property (strong, nonatomic) UIWindow *window;



@end


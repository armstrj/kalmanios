//
//  Filter.hpp
//  Kalman
//
//  Created by jarm on 1/30/17.
//  Copyright © 2017 digb. All rights reserved.
//

#ifndef Filter_hpp
#define Filter_hpp

#include <stdio.h>


#include "CompGeom/Comp_geom_inc.hpp"
#include "CompGeom/Kalman.hpp"


class Filter {
	Filter();
public:
	static Filter& inst();
private:
	Vect<2> g;
	Kalman<3, 1> *kalman_;
};


#endif /* Filter_hpp */

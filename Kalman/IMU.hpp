//
//  IMU.hpp
//  IMU
//
//  Created by Justin Armstrong on 5/28/19.
//  Copyright © 2019 digb. All rights reserved.
//

#ifndef IMU_hpp
#define IMU_hpp

#include <stdio.h>
#include "CompGeom/Comp_geom_inc.hpp"

#define n_msr 3
#define n_param 6

extern const double g_update_interval;

// n_param is number of state parameters being tracked, n_msr is number of parmaeters being measured
class IMU {
public:
    IMU();
    void reset();
    void update_state(const Vect<n_msr>& measurements);
    const Vect<n_param> get_state() { return x; }
private:
    Matx<n_param, n_param> F, F_t;// state transition matrix
    Matx<n_msr, n_param> H;// map measurment to state
    Matx<n_param, n_msr> H_t;// H'
    Vect<n_param> x;// state vector
    Matx<n_param, n_param> P;// P = E[e*e'] state noise estemate e (covariance of the model state)
    Matx<n_param, n_param> Q;// Q = E[w*w'] model noise w (covariance of the unknown inputs)
    Matx<n_msr, n_msr> R;// R = E[v*v'] measurement noise v (covariance of the measurements)
};


#endif /* IMU_hpp */

//
//  ViewController.m
//  IMU
//
//  Created by jarm on 5/16/16.
//  Copyright © 2016 digb. All rights reserved.
//

#import "ViewController.h"
#include "CompGeom/Comp_geom_inc.hpp"

#include "IMU.hpp"


static Vect<3> g_acc_off;
static Vect<3> g_rot_off;
static Vect<3> g_rot_sum;
static bool g_first_acc = true;
static bool g_first_rot = true;

Least_squares<double> g_running_stats_x;

IMU g_imu;

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	
	
	
	self.motionManager = [[CMMotionManager alloc] init];
	self.motionManager.accelerometerUpdateInterval = g_update_interval;
	self.motionManager.gyroUpdateInterval = g_update_interval;
	
	[self.motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue]
                                             withHandler:^(CMAccelerometerData  *accelerometerData, NSError *error) {
												 [self outputAccelertionData:accelerometerData.acceleration];
												 if(error){
													 
													 NSLog(@"%@", error);
												 }
											 }];
	
	[self.motionManager startGyroUpdatesToQueue:[NSOperationQueue currentQueue]
									withHandler:^(CMGyroData *gyroData, NSError *error) {
										[self outputRotationData:gyroData.rotationRate];
									}];
    
}

static int g_upd_int = 10;

- (void)outputAccelertionData:(CMAcceleration)acceleration {
	
    static int acc_ct = 0;
    
    const double g = 9.8;
    const Vect<3> total_acc(acceleration.z*g, acceleration.x*g, acceleration.y*g);
    
    if (g_first_acc) {
        g_acc_off = total_acc;
        g_first_acc = false;
    }
    
    g_running_stats_x.update(total_acc[0]);
    
    const Vect<3> acc = total_acc -g_acc_off;
    //acc.print("acc");
    g_imu.update_state(acc);
    
    const Vect<3> y(0.0, 1.0, 0.0);
    const Vect<3> z(0.0, 0.0, 1.0);
    const Vect<3> rt_side_n = cross(total_acc, z).get_normalized();
    const Vect<3> inc_cross = cross(total_acc, z);
    const double sin_inc = rt_side_n.dot(inc_cross);
    const double cos_inc = total_acc.dot(z);
    const double inc = atan2(sin_inc, cos_inc);
    
    // find rool angle (angle highside to x)
    const Vect<3> roll_cross = cross(rt_side_n, y);
    const double sin_roll = z.dot(roll_cross);
    const double cos_roll = rt_side_n.dot(y);
    const double roll = atan2(sin_roll, cos_roll);
    
    Vect<6> state = g_imu.get_state();
    
    if ((acc_ct % g_upd_int) != 0) {
        acc_ct ++;
        return;
    }
    acc_ct ++;
    
    self.accX.text = [NSString stringWithFormat:@" %.4fg", acceleration.x];
    self.accY.text = [NSString stringWithFormat:@" %.4fg", acceleration.y];
    self.accZ.text = [NSString stringWithFormat:@" %.4fg", acceleration.z];
    
    self.incRaw.text = [NSString stringWithFormat:@" %.4g", inc * 180 / M_PI];
    self.rollRaw.text = [NSString stringWithFormat:@" %.4g", roll * 180 / M_PI];
    self.xVar.text = [NSString stringWithFormat:@" %.4g", g_running_stats_x.std()];
    
    
    
    self.xVel.text = [NSString stringWithFormat:@" %.4g", state[0]];
    self.yVel.text = [NSString stringWithFormat:@" %.4g", state[2]];
    self.zVel.text = [NSString stringWithFormat:@" %.4g", state[4]];
}


- (void)outputRotationData:(CMRotationRate)rotation {
    
    static int rot_ct = 0;
    
    const Vect<3> raw_rot(rotation.x, rotation.y, rotation.z);
    const Vect<3> rot_deg = raw_rot * (180.0 / M_PI);
    
    if (g_first_rot) {
        g_rot_off = rot_deg;
        g_first_rot = false;
    }
    
    g_rot_sum += (rot_deg -g_rot_off) * g_update_interval;
    
    if ((rot_ct % g_upd_int) != 0) {
        rot_ct ++;
        return;
    }
    rot_ct ++;
	
	self.rotX.text = [NSString stringWithFormat:@" %.4fd/s", rot_deg[0]];
	self.rotY.text = [NSString stringWithFormat:@" %.4fd/s", rot_deg[1]];
	self.rotZ.text = [NSString stringWithFormat:@" %.4fd/s", rot_deg[2]];
    
    self.rotXsum.text = [NSString stringWithFormat:@" %.4fd/s", g_rot_sum[0]];
    self.rotYsum.text = [NSString stringWithFormat:@" %.4fd/s", g_rot_sum[1]];
    self.rotZsum.text = [NSString stringWithFormat:@" %.4fd/s", g_rot_sum[2]];
    
    
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (IBAction)resetFilter:(id)sender {
	
    g_first_acc = true;
    g_first_rot = true;
    g_rot_sum = 0.0;
    
    g_imu.reset();
    
    g_running_stats_x.clear();
	
}

@end

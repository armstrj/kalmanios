//
//  IMU.cpp
//  IMU
//
//  Created by Justin Armstrong on 5/28/19.
//  Copyright © 2019 digb. All rights reserved.
//

#include "IMU.hpp"

const double g_update_interval = 0.01;


IMU::IMU()
{
    
    const double sig_a = 0.1;
    const double sig_msr = 0.0012;
    const double d_t = g_update_interval;
    const double sig_state[n_param] = {32.0, 9.8, 32.0, 9.8, 32.0, 9.8};
    const double sig_proc[n_param] = {sig_a*d_t, sig_a, sig_a*d_t, sig_a, sig_a*d_t, sig_a};
    const double sig_meas[n_msr] = {sig_msr, sig_msr, sig_msr};
    const double st_trans[n_param*n_param] = {1.0, d_t, 0.0, 0.0, 0.0, 0.0,
                                              0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
                                              0.0, 0.0, 1.0, d_t, 0.0, 0.0,
                                              0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
                                              0.0, 0.0, 0.0, 0.0, 1.0, d_t,
                                              0.0, 0.0, 0.0, 0.0, 0.0, 1.0};
    const double state_to_measure_transform[n_msr*n_param] = {0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
                                                              0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
                                                              0.0, 0.0, 0.0, 0.0, 0.0, 1.0};

    // compute inital process covarience P
    // only diaginal entries assuming that the inital varience estimates are uncorilated
    for (unsigned i=0; i<n_param; i++) {
        P[i][i] = sig_state[i] * sig_state[i];
    }
    
    // compute the process noise
    // process noise could be for instance the effect of the uncertenry in acceleration over the sampling interval dt
    // compute process covarience P
    // only diaginal entries assuming that the inital varience estimates are uncorilated
    for (unsigned i=0; i<n_param; i++) {
        Q[i][i] = sig_proc[i] * sig_proc[i];
    }
    
    
    // compute the measurement covarience
    // assuming that measurements are uncorilated
    for (unsigned i=0; i<n_msr; i++) {
        R[i][i] = sig_meas[i] * sig_meas[i];
    }
    
    // state transition
    for (unsigned i=0; i<n_param; i++) {
        for (unsigned j=0; j<n_param; j++) {
            F[i][j] = st_trans[n_param*i +j];
            F_t[j][i] = st_trans[n_param*i +j];
        }
    }
    
    // measurement space to state space transform
    for (unsigned i=0; i<n_msr; i++) {
        for (unsigned j=0; j<n_param; j++) {
            H[i][j] = state_to_measure_transform[n_param*i +j];
            H_t[j][i] = state_to_measure_transform[n_param*i +j];
        }
    }
}

void IMU::reset() {
    x = 0.0;
    
    const double sig_state[n_param] = {32.0, 9.8};
    // compute inital process covarience P
    // only diaginal entries assuming that the inital varience estimates are uncorilated
    for (unsigned i=0; i<n_param; i++) {
        P[i][i] = sig_state[i] * sig_state[i];
    }
}

#pragma mark -
#pragma mark update state

// IMU gain : K = P*H_t*(H*P*H_t +R)
// update state estimate : x = x +K*(z -H*x)
// update state covariance estimate : P = (I -KH)*P
// project in to the next time frame x = F*x, and P = F*P*F_t +Q

void IMU::update_state(const Vect<n_msr>& z) {
    
    const Matx<n_param, n_param> I = identity<n_param>();
    
    // project in to the current time frame
    x = F*x;// project the state x in to the current time frame with F the state transform matrix
    P = F*P*F_t +Q;// project the covariance P in to the current time frame with F and F_t and add process covariance Q (Q is the covariance of the unknown inputs)
    
    // compute the IMU gain
    // K = P*H_T*(H*P*H_t) K = P*H_t*S_1
    Matx<n_msr, n_msr> S_1 = H*P*H_t +R;// innovation covariance
    S_1.invert();
    
    // IMU gain
    const Matx<n_param, n_msr> K = P*H_t*S_1;
    
    // update with measurements
    Vect<n_msr> y = z -H*x;// innovation
    x = x +K*y;
    P = (I -K*H)*P;
}





